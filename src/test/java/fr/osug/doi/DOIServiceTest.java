/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Project;
import fr.osug.doi.repository.DoiRepository;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.repository.ProjectRepository;
import fr.osug.doi.service.DoiService;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles({"test", "debug"})
public class DOIServiceTest {

    private final static Logger logger = LoggerFactory.getLogger(DOIServiceTest.class.getName());

    // members:
    @Autowired
    DoiService doiService;

    @Transactional
    @Test
    public void testCreateProject() throws Exception {
        final Date now = new Date();
        for (int i = 0; i < 13; i++) {
            Assert.assertNotNull(doiService.getOrCreateProject("ProjectTest " + (int) (100.0 * Math.random()), now));
            Thread.sleep(20l);
        }

        final ProjectRepository pr = doiService.getProjectRepository();

        final List<String> names = pr.findAllNamesOrderByNameAsc();
        logger.info("names: {}", names);

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        for (Project dp : projects) {
            logger.info("remove project: {}", dp);
            pr.delete(dp);
        }

        Assert.assertNotNull(doiService.getOrCreateProject("ProjectTest", now));
    }

    @Test
    @Transactional
    public void testRollbackProject() throws Exception {
        final Date now = new Date();
        try {
            for (int i = 0; i < 13; i++) {
                Assert.assertNotNull(doiService.getOrCreateProject("ProjectTest " + (int) (100.0 * Math.random()), now));
                Thread.sleep(20l);
            }

            logger.info("rollback ?");

            throw new RuntimeException("rollback ?");
        } catch (Exception e) {
            logger.info("exception: ", e.getMessage());
        }
    }

    @BeforeTransaction
    public void verifyInitialDatabaseState() {
        logger.info("verifyInitialDatabaseState: start");

        final ProjectRepository pr = doiService.getProjectRepository();

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        for (Project dp : projects) {
            logger.info("remove project: {}", dp);
            pr.delete(dp);
        }
        logger.info("verifyInitialDatabaseState: exit");
    }

    @AfterTransaction
    public void verifyFinalDatabaseState() {
        logger.info("verifyFinalDatabaseState: start");

        final ProjectRepository pr = doiService.getProjectRepository();

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        for (Project dp : projects) {
            logger.info("project: {}", dp);
        }

        Assert.assertTrue(projects.isEmpty());

        logger.info("verifyFinalDatabaseState: exit");
    }

    @Transactional
    @Test
    public void testCreateProjectAndDoi() throws Exception {
        final String projectName = "AMMA-CATCH";

        final Date now = new Date();

        final Project project = doiService.getOrCreateProject(projectName, now);
        Assert.assertNotNull(project);

        for (int i = 0; i < 93; i++) {
            Assert.assertNotNull(doiService.createOrUpdateDoiStaging(project,
                    "AMMA-CATCH.TEST." + (int) (10.0 * Math.random()),
                    "description ...",
                    "md5", false, "log:blah\nblah", now));

            Thread.sleep(20l);
        }

        doiService.updateProjectDate(projectName, now);

        // Query
        final DoiRepository dr = doiService.getDoiRepository();
        final List<Doi> dois = dr.findByProject(projectName);

        logger.info("Dois for project[{}]: {}", projectName, dois.size());

        for (Doi d : dois) {
            logger.info("Doi: {}", d);
        }

        final DoiStagingRepository dsr = doiService.getDoiStagingRepository();
        final List<DoiStaging> doiStagings = dsr.findByProject(projectName);

        logger.info("DoiStaging for project[{}]: {}", projectName, dois.size());

        for (DoiStaging d : doiStagings) {
            logger.info("DoiStaging: {}", d);
        }
    }
}
