/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.xml.validator;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple class to contain XML validation status and messages
 */
public class ValidationResult {

    /** valid flag */
    private boolean valid = false;
    /** validation messages */
    private final List<ErrorMessage> messages = new ArrayList<ErrorMessage>();

    /**
     * Constructor
     */
    public ValidationResult() {
        /* no-op */
    }

    /**
     * Return the validation messages
     *
     * @return validation messages
     */
    public List<ErrorMessage> getMessages() {
        return messages;
    }

    /**
     * Return the valid flag
     *
     * @return valid flag
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Define the valid flag
     *
     * @param pValid valid flag
     */
    protected void setValid(final boolean pValid) {
        this.valid = pValid;
    }
}
