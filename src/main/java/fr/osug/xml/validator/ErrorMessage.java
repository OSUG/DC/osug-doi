/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.xml.validator;

import java.io.Serializable;

/**
 * Xml validation error message
 */
public final class ErrorMessage implements Serializable {

    /** serial UID for Serializable interface */
    private static final long serialVersionUID = 1L;

    /** severity */
    private final SEVERITY severity;
    /** line number */
    private final int lineNumber;
    /** column number */
    private final int columnNumber;
    /** message */
    private final String message;

    /**
     * Constructor
     * @param pSeverity severity of the validation error
     * @param pLineNumber line number in the input document
     * @param pColumnNumber column number in the input document
     * @param pMessage error message
     */
    public ErrorMessage(final SEVERITY pSeverity, final int pLineNumber, final int pColumnNumber, final String pMessage) {
        this.severity = pSeverity;
        this.lineNumber = pLineNumber;
        this.columnNumber = pColumnNumber;
        this.message = pMessage;
    }

    @Override
    public String toString() {
        return this.severity + " [" + this.lineNumber + " : " + this.columnNumber + "] " + this.message;
    }

    /**
     * @return column number
     */
    public int getColumnNumber() {
        return columnNumber;
    }

    /**
     * @return line number
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return severity
     */
    public SEVERITY getSeverity() {
        return severity;
    }

    /**
     * Error severity enum
     */
    public enum SEVERITY {
        /** WARNING */
        WARNING,
        /** ERROR */
        ERROR,
        /** FATAL */
        FATAL;
    }
}
