/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.IndexEntry;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class IndexUtil {

    private final static Logger logger = LoggerFactory.getLogger(IndexUtil.class.getName());

    IndexUtil() {
        // private
    }

    public static void write(final List<IndexEntry> entries, final StringBuilder sb) {
        sb.setLength(0);
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<index>\n");

        for (IndexEntry ps : entries) {
            sb.append("<item>\n");
            sb.append("<name>").append(ps.getName()).append("</name>\n");
            sb.append("<description>");
            DoiCsvToXml.xmlEscapeText(ps.getDesc(), sb);
            sb.append("</description>\n");
            sb.append("<update>").append(ps.getUpdateDate().toString()).append("</update>\n");
            if (ps.getCount() >= 0L) {
                sb.append("<count>").append(ps.getCount()).append("</count>\n");
            }
            sb.append("</item>\n");
        }
        sb.append("</index>\n");
    }

    public static void writeReport(final List<DoiStaging> entries, final StringBuilder sb) {
        sb.setLength(0);
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<report>\n");

        for (DoiStaging ds : entries) {
            final Doi doi = ds.getDoi();

            sb.append("<doi>\n");

            sb.append("<identifier>").append(doi.getIdentifier()).append("</identifier>\n");
            sb.append("<description>");
            DoiCsvToXml.xmlEscapeText(doi.getDescription(), sb);
            sb.append("</description>\n");
            // STAGING / PUBLIC:
            sb.append("<status>").append(doi.getStatus()).append("</status>\n");

            sb.append("<landingExtUrl>");
            DoiCsvToXml.xmlEscapeText(ds.getLandingExtUrl(), sb);
            sb.append("</landingExtUrl>\n");

            sb.append("<valid>").append(ds.isCompletelyValid()).append("</valid>\n");
            sb.append("<metadataMd5>").append(ds.getMetadataMd5()).append("</metadataMd5>\n");

            sb.append("<landingLocUrl>");
            DoiCsvToXml.xmlEscapeText(ds.getLandingLocUrl(), sb);
            sb.append("</landingLocUrl>\n");

            sb.append("<createDate>").append(ds.getCreateDate()).append("</createDate>\n");
            sb.append("<updateDate>").append(ds.getUpdateDate()).append("</updateDate>\n");

            sb.append("<log>");
            DoiCsvToXml.xmlEscapeText(ds.getLog(), sb);
            DoiCsvToXml.xmlEscapeText(ds.getUrlLog(), sb);
            sb.append("</log>\n");

            sb.append("</doi>\n");
        }
        sb.append("</report>\n");
    }

}
