/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
public class PipelineCommonData {

    public static final int BUFFER_SIZE = 4 * 1024;

    /* members */
    /** path config */
    private final PathConfig pathConfig;

    final Date now = new Date();
    /** temporary StringBuilder to hold xml document */
    final StringBuilder buffer = new StringBuilder(BUFFER_SIZE);
    /** project config instances keyed by project name */
    private final Map<String, ProjectConfig> projectConfigs = new LinkedHashMap<String, ProjectConfig>();
    /** all project config instances keyed by project name (dynamically filled) */
    private final Map<String, ProjectConfig> projectConfigsAll = new LinkedHashMap<String, ProjectConfig>();
    /** optional doi pattern */
    private String doiPattern = null;

    public PipelineCommonData(final PathConfig pathConfig) {
        this.pathConfig = pathConfig;
    }

    public void addProjectConfig(final ProjectConfig projectConfig) {
        projectConfigs.put(projectConfig.getProjectName(), projectConfig);
        addProjectConfigAll(projectConfig);
    }

    public void removeProjectConfig(final ProjectConfig projectConfig) {
        projectConfigs.remove(projectConfig.getProjectName());
        removeProjectConfigAll(projectConfig);
    }

    public Collection<ProjectConfig> getProjectConfigs() {
        return projectConfigs.values();
    }

    public void addProjectConfigAll(final ProjectConfig projectConfig) {
        projectConfigsAll.put(projectConfig.getProjectName(), projectConfig);
    }

    public void removeProjectConfigAll(final ProjectConfig projectConfig) {
        projectConfigsAll.remove(projectConfig.getProjectName());
    }

    public Collection<ProjectConfig> getProjectConfigsAll() {
        return projectConfigsAll.values();
    }

    public ProjectConfig getProjectConfigAll(final String projectName) throws IOException {
        ProjectConfig projectConfig = projectConfigsAll.get(projectName);
        if (projectConfig == null) {
            projectConfig = new ProjectConfig(pathConfig, projectName);
            addProjectConfigAll(projectConfig);
        }
        return projectConfig;
    }

    public String getDoiPattern() {
        return doiPattern;
    }

    public void setDoiPattern(String doiPattern) {
        this.doiPattern = doiPattern;
    }

}
