/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.repository;

import fr.osug.doi.domain.DoiPublic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the DoiPublic entity.
 */
@Repository
@Transactional
public interface DoiPublicRepository extends DoiBaseRepository<DoiPublic>, JpaRepository<DoiPublic, Long> {

}
