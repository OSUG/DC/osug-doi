/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class DoiCsvSplitter {

    private DoiCsvSplitter() {
        // forbidden
    }

    public static List<DoiCsvData> split(final File csvFile) throws IOException {
        final CsvData data = CsvUtil.read(csvFile);

        final List<DoiCsvData> files = new ArrayList<DoiCsvData>();

        if (!data.isEmpty()) {
            // split blocks:
            final List<String[]> rows = data.getRows();

            List<String[]> extracted = null;

            for (int i = 0, size = rows.size(); i < size; i++) {
                final String[] cols = rows.get(i);
                if (cols != null) {
                    if (cols.length >= 2) {
                        // check block delimiters: BEGIN END
                        final String key = cols[0];

                        if (key.isEmpty()) {
                            continue;
                        }

                        if (key.charAt(0) == Const.COMMENT) {
                            if ("# BEGIN".equals(key) || "# END".equals(key)) {
                                // add part if needed:
                                addPart(files, extracted);
                                extracted = null;
                                continue;
                            }
                            // skip row
                            continue;
                        }
                    }

                    if (extracted == null) {
                        extracted = new ArrayList<String[]>();
                    }
                    extracted.add(cols);
                }
            }
            // add part if needed:
            addPart(files, extracted);
        }
        return files;
    }

    private static void addPart(final List<DoiCsvData> files, final List<String[]> extracted) {
        if (extracted != null && !extracted.isEmpty()) {
            final DoiCsvData part = new DoiCsvData(extracted);
            part.trim();
            files.add(part);
        }
    }
}
