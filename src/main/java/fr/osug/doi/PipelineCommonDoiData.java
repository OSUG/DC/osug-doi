/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.validation.WarningMessage;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class PipelineCommonDoiData {

    private final static Logger logger = LoggerFactory.getLogger(PipelineCommonDoiData.class.getName());

    /* members */
    private final String doiSuffix;
    /** validation messages */
    private final List<WarningMessage> messages = new ArrayList<WarningMessage>();
    /** valid flag */
    private boolean valid;

    PipelineCommonDoiData(final String doiSuffix) {
        this.doiSuffix = doiSuffix;
        this.valid = true;
    }

    /**
     * Add the given message to the error messages
     * @param msg message to add
     */
    public final void addError(final String msg) {
        setValid(false);
        logger.error(msg);
        messages.add(new WarningMessage(msg, WarningMessage.Level.Error));
    }

    /**
     * Add the given message to the warning messages
     * @param msg message to add
     */
    public final void addWarning(final String msg) {
        setValid(false);
        logger.warn(msg);
        messages.add(new WarningMessage(msg, WarningMessage.Level.Warning));
    }

    /**
     * Add the given message to the information messages
     * @param msg message to add
     */
    public final void addInfo(final String msg) {
        logger.info(msg);
        messages.add(new WarningMessage(msg, WarningMessage.Level.Information));
    }

    public final String messagesToString() {
        if (messages.isEmpty()) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(1024);
        for (WarningMessage msg : messages) {
            sb.append(msg.getLevel()).append(" : ");
            sb.append(msg.getMessage()).append("\n");
        }
        return sb.toString();
    }

    public final String getDoiSuffix() {
        return doiSuffix;
    }

    public final boolean isValid() {
        return valid;
    }

    public final void setValid(final boolean valid) {
        this.valid = valid;
    }

}
