/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.io.File;

/**
 *
 * @author bourgesl
 */
public final class FilePathUtils {

    /**
     * Return the doi metadata (xml) file into the data/staging directory:
     * @param projectConfig project config
     * @param isStaging staging or public
     * @param doiSuffix DOI suffix
     * @return doi metadata (xml) file
     */
    public static File getDoiMetaDataFile(final ProjectConfig projectConfig, final boolean isStaging, final String doiSuffix) {
        final File doiDir = (isStaging) ? projectConfig.getStagingDir() : projectConfig.getPublicDir();
        return new File(doiDir, doiSuffix + Const.FILE_EXT_XML);
    }
}
