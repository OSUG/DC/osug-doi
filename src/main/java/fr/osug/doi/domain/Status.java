/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

/**
 * The Status enumeration.
 */
public enum Status {
    STAGING,
    PUBLIC
}
