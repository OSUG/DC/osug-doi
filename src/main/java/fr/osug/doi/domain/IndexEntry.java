/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

import java.util.Date;

/**
 *
 */
public final class IndexEntry {

    private final String name;
    private final String desc;
    private final Date updateDate;
    private final long count;

    public IndexEntry(String name, String desc, Date updateDate) {
        this(name, desc, updateDate, -1L);
    }

    public IndexEntry(String name, String desc, Date updateDate, long count) {
        this.name = name;
        this.desc = desc;
        this.updateDate = updateDate;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "IndexEntry{" + "name=" + name + ", desc=" + desc + ", updateDate=" + updateDate + ", count=" + count + '}';
    }

}
