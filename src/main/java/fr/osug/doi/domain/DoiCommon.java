/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.data.domain.Persistable;

/**
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DoiCommon implements Persistable<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "doi_common_id_seq", sequenceName = "doi_common_id_seq", initialValue = 1, allocationSize = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "doi_common_id_seq")
    private Long id;

    @OneToOne
    @JoinColumn(name = "doi_id")
    private Doi doi;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "metadata_md5")
    private String metadataMd5;

    /* url fields */
    @Column(name = "data_access_url")
    private String dataAccessUrl;
    
    @Column(name = "data_access_alt_url")
    private String dataAccessAltUrl;

    @Column(name = "landing_ext_url")
    private String landingExtUrl;

    @Column(name = "landing_loc_url")
    private String landingLocUrl;

    /* datacite fields */
    @Column(name = "dc_metadata_md5")
    private String dataciteMetadataMd5;

    @Column(name = "dc_url")
    private String dataciteUrl;

    @Override
    public final Long getId() {
        return id;
    }

    @Override
    public final boolean isNew() {
        return id == null;
    }

    public final Doi getDoi() {
        return doi;
    }

    public final void setDoi(Doi doi) {
        this.doi = doi;
    }

    public final Date getCreateDate() {
        return createDate;
    }

    public final void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public final Date getUpdateDate() {
        return updateDate;
    }

    public final void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }

    public final String getMetadataMd5() {
        return metadataMd5;
    }

    public final void setMetadataMd5(final String metadataMd5) {
        this.metadataMd5 = metadataMd5;
    }

    public String getDataAccessUrl() {
        return dataAccessUrl;
    }

    public void setDataAccessUrl(String dataAccessUrl) {
        this.dataAccessUrl = dataAccessUrl;
    }

    public String getDataAccessAltUrl() {
        return dataAccessAltUrl;
    }

    public void setDataAccessAltUrl(String dataAccessAltUrl) {
        this.dataAccessAltUrl = dataAccessAltUrl;
    }

    public String getLandingExtUrl() {
        return landingExtUrl;
    }

    public void setLandingExtUrl(String landingExtUrl) {
        this.landingExtUrl = landingExtUrl;
    }

    public final String getLandingLocUrl() {
        return landingLocUrl;
    }

    public final void setLandingLocUrl(final String landingLocUrl) {
        this.landingLocUrl = landingLocUrl;
    }

    public String getDataciteMetadataMd5() {
        return dataciteMetadataMd5;
    }

    public void setDataciteMetadataMd5(String dataciteMetadataMd5) {
        this.dataciteMetadataMd5 = dataciteMetadataMd5;
    }

    public String getDataciteUrl() {
        return dataciteUrl;
    }

    public void setDataciteUrl(String dataciteUrl) {
        this.dataciteUrl = dataciteUrl;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.doi);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DoiCommon other = (DoiCommon) obj;
        if (!Objects.equals(this.doi, other.doi)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "id=" + id
                + ", doi=[" + doi + "]"
                + ", createDate='" + createDate + "'"
                + ", updateDate='" + updateDate + "'"
                + ", metadataMd5='" + metadataMd5 + "'"
                + ", dataAccessUrl='" + dataAccessUrl + "'"
                + ", dataAccessAltUrl='" + dataAccessAltUrl + "'"
                + ", landingExtUrl='" + landingExtUrl + "'"
                + ", landingLocUrl='" + landingLocUrl + "'"
                + ", dataciteMetadataMd5='" + dataciteMetadataMd5 + "'"
                + ", dataciteUrl='" + dataciteUrl + "'";
    }
}
