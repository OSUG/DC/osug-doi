/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.DoiCommon;
import fr.osug.doi.repository.DoiBaseRepository;
import fr.osug.doi.service.DataciteClient;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class DataciteSyncPipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(DataciteSyncPipeline.class.getName());

    // members
    private final boolean doStaging;
    private final boolean doPublic;
    private List<String> publishedDois = null;

    public DataciteSyncPipeline(final boolean doStaging,
                                final boolean doPublic,
                                final PipelineCommonData pipeData,
                                final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
        this.doStaging = doStaging;
        this.doPublic = doPublic;
    }

    @Override
    public void doExecute() throws IOException {
        final Collection<ProjectConfig> projectConfigs = pipeData.getProjectConfigs();

        if (projectConfigs.isEmpty()) {
            logger.info("publishDatacite: skipping (no project defined)");
        } else {
            logger.info("doStaging: {}", doStaging);
            logger.info("doPublic:  {}", doPublic);

            prepare();

            for (ProjectConfig projectConfig : projectConfigs) {
                publishDatacite(projectConfig);
            }
        }
    }

    private void prepare() {
        if (doiConfig.isDataciteClientEnabled()) {
            final DataciteClient dc = doiConfig.getDataciteClient();
            this.publishedDois = dc.getDois();

            logger.debug("Datacite DOI list:\n{}", publishedDois);
        }
    }

    private void publishDatacite(final ProjectConfig projectConfig) throws IOException {
        logger.info("publishDatacite ...");
        if (doStaging) {
            if (false) {
                // 2019.6: test prefix disabled on datacite.org
                // skip datacite publication for staging DOIs
                publishDatacite(projectConfig, true);
            }
        }
        if (doPublic) {
            publishDatacite(projectConfig, false);
        }
        logger.info("publishDatacite: done.");
    }

    private void publishDatacite(final ProjectConfig projectConfig,
                                 final boolean isStaging) throws IOException {

        final DataciteClient dc = doiConfig.getDataciteClient();

        final String doiPrefix = ((isStaging) ? Const.DOI_PREFIX_TEST : doiConfig.getPrefix()) + '/';
        final String urlBase = doiConfig.getDomain() + '/' + Paths.DIR_WEB_REDIRECT + '/';

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();
        final DoiBaseRepository<?> dbr = doiService.getDoiCommonRepository(isStaging);

        final List<? extends DoiCommon> dList = dbr.findByProject(projectName);

        logger.debug("DoiCommon list for project[{}]: {}", projectName, dList);

        for (DoiCommon d : dList) {
            logger.debug("DoiCommon: {}", d);

            final String doiSuffix = d.getDoi().getIdentifier();

            // complete DOI:
            final String doi = doiPrefix + doiSuffix;
            // datacite url:
            final String url = urlBase + doiSuffix;

            final File doiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, isStaging, doiSuffix);

            if (!doiFile.exists()) {
                if (isStaging) {
                    logger.info("Ignoring {} - no xml", doiSuffix);
                } else {
                    logger.warn("Ignoring {} - missing xml at {}", doiSuffix, doiFile);
                }
            } else {
                if (!doiConfig.isDataciteClientEnabled()) {
                    logger.warn("skipping DOI [{}] - datacite client disabled", doi);
                } else {
                    // only skip staging DOI:
                    // TODO: consider for public once it is tested:
                    final boolean exist = (isStaging && publishedDois != null) ? publishedDois.contains(doi.toUpperCase()) : false;

                    boolean push = true;

                    if (exist) {
                        // verify that URL and metadata (MD5) are equivalent:
                        if (url.equals(d.getDataciteUrl()) && d.getMetadataMd5().equals(d.getDataciteMetadataMd5())) {
                            push = false;
                        }
                    }

                    if (push) {
                        logger.info("publish DOI [{}]", doi);

                        // Push DOI in staging phase:
                        final String metadata = FileUtils.readFile(doiFile);

                        if (dc.publishDoi(doi, url, metadata)) {
                            // update database:
                            if (isStaging) {
                                doiService.updateStagingDoiDatacite(doiSuffix, d.getMetadataMd5(), url, now);
                            } else {
                                doiService.updatePublicDoiDatacite(doiSuffix, d.getMetadataMd5(), url, now);
                            }
                        } else {
                            logger.error("Failed to push DOI[{}] to datacite", doi);
                        }
                    } else {
                        logger.info("skipping DOI [{}] - datacite is up-to-date", doi);
                    }
                }
            }
        }
    }
}
