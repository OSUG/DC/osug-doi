/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class ProcessPipelineDoiData extends PipelineCommonDoiData {

    private final static Logger logger = LoggerFactory.getLogger(ProcessPipelineDoiData.class.getName());

    /* members */
    private final String doiId;
    /** doi title */
    private final String title;
    /** md5 metadata */
    private String metadataMd5;

    ProcessPipelineDoiData(final String doiId, final String doiSuffix, final String title) {
        super(doiSuffix);
        this.doiId = doiId;
        this.title = title;
    }

    public String getDoiId() {
        return doiId;
    }

    public String getTitle() {
        return title;
    }

    public String getMetadataMd5() {
        return metadataMd5;
    }

    public void setMetadataMd5(final String metadataMd5) {
        this.metadataMd5 = metadataMd5;
    }

}
