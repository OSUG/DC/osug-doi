/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public final class DoiCsvData extends CsvData {

    static final Set<String> KEY_SET = new HashSet<String>(128);
    static final Set<String> KEY_ATTR_SET = new HashSet<String>(32);
    static final Map<String, Integer> KEY_ORDER_INDEX = new HashMap<String, Integer>(128);

    static {
        int i = 0;
        // supported keys:
        for (String key : Const.KEY_ORDER) {
            KEY_SET.add(key);
            KEY_ORDER_INDEX.put(key, Integer.valueOf(i++));
        }
        // supported attributes (extra CSV columns):
        KEY_ATTR_SET.addAll(Arrays.asList(Const.KEY_ATTRS_NAMES));
        KEY_ATTR_SET.addAll(Arrays.asList(Const.KEY_ATTRS_RIGHTS));
        KEY_ATTR_SET.addAll(Arrays.asList(Const.KEY_ATTRS_FUNDER));
    }

    DoiCsvData(final List<String[]> rows) {
        super(rows);
    }

    DoiCsvData(final CsvData data) {
        super(data.filePath, data.rows);
    }

    public String getDoiId() {
        return getValue(Const.KEY_IDENTIFIER);
    }

    public String getDoiSuffix() {
        return getDoiSuffix(getDoiId());
    }

    public String getFirstGeoLocationPlace() {
        return getValue(Const.KEY_GEO_LOCATION_PLACE);
    }

    public String getTitle() {
        final String title = getValue(Const.KEY_TITLE);
        return (title != null) ? title : "";
    }

    public Set<String> getReferences() {
        Set<String> refs = null;
        for (String[] cols : rows) {
            if (cols != null && cols.length >= 2) {
                final String key = cols[0];
                if (key.startsWith(Const.KEY_REL_ID_START) && key.endsWith(Const.KEY_REL_ID_DOI)) {
                    if (refs == null) {
                        refs = new LinkedHashSet<String>();
                    }
                    refs.add(cols[1]);
                }
            }
        }
        return refs;
    }

    public void trim() {
        for (ListIterator<String[]> it = rows.listIterator(); it.hasNext();) {
            final String[] cols = it.next();
            if (cols == null || cols.length == 0) {
                // remove line:
                it.remove();
                continue;
            }
            // Find first empty value:
            int last = -1;
            for (int i = 0; i < cols.length; i++) {
                String v = cols[i].trim();
                if (v.isEmpty() || v.charAt(0) == Const.COMMENT) {
                    break;
                }

                // Check keys:
                if (i % 2 == 0) {
                    if (!KEY_SET.contains(v) && ((i == 0) || !KEY_ATTR_SET.contains(v))) {
                        if (!shouldIgnoreKey(v)) {
                            logger.warn("Invalid key found [{}] for row: {}", v, Arrays.toString(cols));
                        }
                        // ignore remaining columns:
                        break;
                    }
                }

                last = i;
            }

            int n = last + 1;
            if (n < 2) {
                // first pair is invalid:
                if (logger.isDebugEnabled()) {
                    logger.debug("trim: invalid first pair: {}", Arrays.toString(cols));
                }
                // remove line:
                it.remove();
                continue;
            }

            if (n % 2 != 0) {
                // ignore invalid [key|value] pairs (ie extra column for comments):
                if (logger.isDebugEnabled()) {
                    logger.debug("trim: even columns: {}", Arrays.toString(cols));
                }
                n -= 1;
            }

            if (n != cols.length) {
                // resize array:
                final String[] newCols = Arrays.copyOf(cols, n);
                if (logger.isDebugEnabled()) {
                    logger.debug("trim: resize columns: {} to: {}", Arrays.toString(cols), Arrays.toString(newCols));
                }
                it.set(newCols);
            }
        }

        logger.debug("trim: {}", this);
    }

    private static boolean shouldIgnoreKey(final String value) {
        for (int i = 0; i < Const.KEY_IGNORE.length; i++) {
            if (value.startsWith(Const.KEY_IGNORE[i])) {
                return true;
            }
        }
        return false;
    }

    public void mergeFrom(final DoiCsvData src) {
        for (String[] srcCols : src.getRows()) {
            if (srcCols != null) {
                // look for matching in this CSV:
                boolean match = false;
                for (String[] toCols : rows) {
                    if (Arrays.equals(srcCols, toCols)) {
                        match = true;
                        break;
                    }
                }

                if (!match) {
                    // avoid duplicates
                    rows.add(srcCols);
                }
            }
        }
    }

    public void sort() {
        Collections.sort(rows, RowComparator.INSTANCE);
    }

    public static String getDoiSuffix(final String doi) {
        final int pos = doi.indexOf('/');

        if (pos != -1) {
            return doi.substring(pos + 1);
        }
        return doi;
    }

    final static class RowComparator implements Comparator<String[]> {

        final static RowComparator INSTANCE = new RowComparator();

        @Override
        public int compare(final String[] row1, final String[] row2) {
            int idx1 = -1;
            int idx2 = -1;
            if (row1 != null && row1.length != 0) {
                idx1 = getIndex(row1[0]);
            }
            if (row2 != null && row2.length != 0) {
                idx2 = getIndex(row2[0]);
            }
            return Integer.compare(idx1, idx2);
        }

        private static int getIndex(final String key) {
            final Integer idx = KEY_ORDER_INDEX.get(key);
            if (idx == null) {
                logger.info("Missing key [{}] !", key);
                return -1;
            }
            return idx.intValue();
        }
    }
}
