/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug;

import ch.qos.logback.classic.Level;
import fr.osug.doi.AbstractPipeline;
import fr.osug.doi.Const;
import fr.osug.doi.DOIConfig;
import fr.osug.doi.GeneratePipeline;
import fr.osug.doi.Paths;
import fr.osug.doi.ProjectConfig;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {EmbeddedServletContainerAutoConfiguration.class, WebMvcAutoConfiguration.class})
@EnableTransactionManagement
public class DOIApplication {

    private final static Logger logger = LoggerFactory.getLogger(DOIApplication.class.getName());

    /**
     * Main entry point
     * @param args command-line arguments
     * @throws Exception 
     */
    public static void main(final String[] args) throws Exception {
        // return exitCode from ExitCodeGenerator:
        System.exit(
                SpringApplication.exit(
                        SpringApplication.run(DOIApplication.class, args)
                )
        );
    }

    // members:
    @Autowired
    private DOIConfig doiConfig;

    @PostConstruct
    public void initialize() {
        // Set the default locale to en-US locale (for Numerical Fields "." ",")
        Locale.setDefault(Locale.US);

        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        logger.info("Default Charset=" + Charset.defaultCharset());

        // may throw IllegalStateException:
        final String basePath = Paths.DIR_BASE;
        logger.info("Base Path=" + basePath);
        
        logger.info("Datacite Schema version: " + Const.SCHEMA_VERSION);
    }

    @PreDestroy
    public void onExit() {
        if (doiConfig.isDebug() && logger.isInfoEnabled()) {
            logger.info("Exit hook: components provided by Spring Boot:");

            final String[] beanNames = doiConfig.getAppCtx().getBeanDefinitionNames();
            Arrays.sort(beanNames);

            for (String beanName : beanNames) {
                logger.info(beanName);
            }
        }
    }

    @Component
    @Profile("!test")
    public static class DOIPipelineRunner implements ApplicationRunner, ExitCodeGenerator {

        private final static String ARG_ACTION = "action";

        // members:
        @Autowired
        private DOIConfig doiConfig;
        /* exit status */
        private int exitCode = 0;

        @Override
        public void run(final ApplicationArguments aa) {
            // Just leave method if no argument has been given
            if (aa.getSourceArgs().length == 0) {
                fail("Missing arguments !");
            }

            if (aa.containsOption("v")) {
                final String levelString = getSingleValue("v", aa);
                setLoggerLevel(LoggerFactory.getLogger("fr.osug.doi"), levelString);
            }

            if (logger.isInfoEnabled()) {
                for (String opt : aa.getOptionNames()) {
                    logger.info("argument [{}] = '{}'", opt, aa.getOptionValues(opt));
                }
            }

            final String action = getRequiredValue(ARG_ACTION, aa);
            switch (action) {
                case "help":
                    showArgumentsHelp();
                    break;
                case "process":
                    doProcess(aa);
                    break;
                case "process-url":
                    doProcessUrls(aa);
                    break;
                case "publish":
                    doPublish(aa);
                    break;
                case "remove":
                    doRemove(aa);
                    break;
                case "generate":
                    doGenerate(aa);
                    break;
                default:
                    if (action != null) {
                        fail("Unsupported action: " + action);
                    }
            }
        }

        private void doProcess(ApplicationArguments aa) {
            // Required values:
            final String project = getRequiredValue("project", aa);
            // Optional values:
            final boolean saveCSV = aa.containsOption("csv");

            try {
                // Staging only
                final AbstractPipeline<?> pipeline = doiConfig.getProcessPipeline(saveCSV, getProjectConfig(project));
                pipeline.execute();

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        private void doProcessUrls(ApplicationArguments aa) {
            // Required values:
            final String project = getRequiredValue("project", aa);

            try {
                // Staging only
                final AbstractPipeline<?> pipeline = doiConfig.getProcessUrlPipeline(getProjectConfig(project), null);
                pipeline.execute();

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        private void doPublish(ApplicationArguments aa) {
            // Required values:
            final String project = getRequiredValue("project", aa);
            final String doiPattern = getRequiredValue("doi", aa);

            try {
                final DataInputStream in = new DataInputStream(System.in);
                System.out.println("Do you confirm to publish matching DOI (y/N) ?");

                final char ch = (char) in.readByte();

                if (Character.toLowerCase(ch) == 'y') {
                    // Public only
                    final AbstractPipeline<?> pipeline = doiConfig.getPublishPipeline(getProjectConfig(project), doiPattern);
                    pipeline.execute();
                } else {
                    logger.info("Action cancelled ({})", ch);
                }

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        private void doRemove(ApplicationArguments aa) {
            // Required values:
            final String project = getRequiredValue("project", aa);
            final String doiPattern = getRequiredValue("doi", aa);

            try {
                final DataInputStream in = new DataInputStream(System.in);
                System.out.println("Do you confirm to remove matching (staging) DOI (y/N) ?");

                final char ch = (char) in.readByte();

                if (Character.toLowerCase(ch) == 'y') {
                    // Staging only
                    final AbstractPipeline<?> pipeline = doiConfig.getRemovePipeline(getProjectConfig(project, false), doiPattern);
                    pipeline.execute();
                } else {
                    logger.info("Action cancelled ({})", ch);
                }

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        private void doGenerate(ApplicationArguments aa) {
            // Optional values:
            final String project = getOptionalValue("project", aa);
            // mode (staging / public)
            final String mode = getOptionalValue("mode", aa);

            final boolean doStaging = (mode == null) || "staging".equalsIgnoreCase(mode) || "all".equalsIgnoreCase(mode);
            final boolean doPublic = "public".equalsIgnoreCase(mode) || "all".equalsIgnoreCase(mode);

            try {
                final GeneratePipeline pipeline = doiConfig.getGeneratePipeline(doStaging, doPublic, null);
                pipeline.execute(project);

            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        private ProjectConfig getProjectConfig(final String project) throws IOException {
            return getProjectConfig(project, true);
        }

        private ProjectConfig getProjectConfig(final String project, final boolean required) throws IOException {
            return new ProjectConfig(doiConfig.getPathConfig(), project, required);
        }

        /** Show command arguments help. */
        public static void showArgumentsHelp() {
            System.out.println("---------------------------------- Arguments help ------------------------------");
            System.out.println("| Key          Value           Description                                     |");
            System.out.println("|------------------------------------------------------------------------------|");
            System.out.println("| --action=[help|process|generate] Define the command to perform               |");
            System.out.println("| [--v=0|1|2|3|4|5]             Define console logging level                   |");
            System.out.println("|                                                                              |");
            System.out.println("| LOG LEVELS : 0 = OFF, 1 = ERROR, 2 = WARNING, 3 = INFO, 4 = DEBUG, 5 = ALL   |");
            System.out.println("|------------------------------------------------------------------------------|");
            System.out.println("| Action [process]:                                                            |");
            System.out.println("| --project=<name>              Project folder name (case sensitive)           |");
            System.out.println("| [--csv]                       CSV Output format                              |");
            System.out.println("| Action [generate]:                                                           |");
            System.out.println("| --project=<name>              Optional Project folder name (case sensitive)  |");
            System.out.println("|------------------------------------------------------------------------------|\n");
        }

        private void fail(String msg) {
            showArgumentsHelp();
            setExitCode(-1);
            throw new IllegalArgumentException(msg);
        }

        @Override
        public int getExitCode() {
            return exitCode;
        }

        private void setExitCode(int exitCode) {
            this.exitCode = exitCode;
        }

        private String getOptionalValue(final String opt, final ApplicationArguments aa) {
            if (aa.containsOption(opt)) {
                return getSingleValue(opt, aa);
            }
            return null;
        }

        private String getRequiredValue(final String opt, final ApplicationArguments aa) {
            if (!aa.containsOption(opt)) {
                fail("Missing argument '" + opt + "' !");
                return null;
            }
            return getSingleValue(opt, aa);
        }

        private static String getSingleValue(final String opt, final ApplicationArguments aa) {
            final List<String> values = aa.getOptionValues(opt);
            if (values.size() != 1) {
                throw new IllegalArgumentException("Too many values for argument '" + opt + "': " + values);
            }
            return values.get(0);
        }

        /**
         * Set given logger's level (if it is a logback logger)
         * @param logger slf4j logger
         * @param levelString level as string
         */
        private static void setLoggerLevel(Logger logger, String levelString) {
            Level level = null;
            switch (levelString) {
                case "0":
                    level = Level.OFF;
                    break;
                case "1":
                    level = Level.ERROR;
                    break;
                case "2":
                    level = Level.WARN;
                    break;
                case "3":
                    level = Level.INFO;
                    break;
                case "4":
                    level = Level.DEBUG;
                    break;
                case "5":
                    level = Level.ALL;
                    break;
                default:
                    logger.info("Invalid Level [{}]", level);
                    showArgumentsHelp();
                    return;
            }
            if (logger instanceof ch.qos.logback.classic.Logger) {
                logger.info("Set logger[{}] Level to [{}]", logger.getName(), level);
                ((ch.qos.logback.classic.Logger) logger).setLevel(level);
            }
        }

    }

}
