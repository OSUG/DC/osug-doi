
A. User management:
1. Create doi system user:

adduser --system --home /srv/doi --shell /bin/bash --group --disabled-login doi

Adding system user `doi' (UID 111) ...
Adding new group `doi' (GID 117) ...
Adding new user `doi' (UID 111) with group `doi' ...
Creating home directory `/srv/doi' ...

Copy .bashrc from bourgesl:
cp /home/bourgesl/.bashrc .
ln -s .bashrc .bash_profile


2. Add your user into the doi group: ??
usermod -a -G doi <user>


To use the doi user, just type:
bourgesl@osug-doi:~$ sudo su -
root@osug-doi:~# su - doi
doi@osug-doi:~$ 


3. Upload a release on the server:
(dev) scp doimgr-trunk-release.tar.gz doi.osug.fr:

And extract it into the doi account (as doi user):
cp /home/<user>/doimgr-trunk-release.tar.gz .
tar xvfz doimgr-trunk-release.tar.gz 

(First time): copy the complete package into osug-doi/:
cp -r doimgr-trunk-release/ osug-doi

check rights (doi|doi) on files

4. Initialize the web root:
cd osug-doi/bin/
./setup_www.sh 

As root, start apache2:
service apache2 start


B. Software packages:
 
1.Install package apache2
    see setup_apache.txt

2.Install package postgres
    see setup_postgres.txt

3. Setup tool:
    - edit osug-doi/bin/application.yml from doc/application.yml (change datacite password)
    - edit osug-doi/bin/application-prod.yml from doc/application-prod.yml (change postgres settings)

    - enable profile 'prod' in env.sh:
# SpringBoot profile: dev or production
#PROFILE="default" # default = h2 db
PROFILE="prod"   # production = postgresql


