#!/bin/bash
# fix input files

#set -eux;

sed -i "s/DOI;10.17178/DOI;10.5072/g" vfs/API/osug-doi/metadata/*

cp vfs/API/osug-doi/doi_url_data_access.csv ../
cp vfs/API/osug-doi/metadata/* ../metadata/


