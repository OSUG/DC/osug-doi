#!/bin/bash

mkdir -p vfs
cd vfs

if [ -d API ]; then
  echo "folder API already exists !"
  exit 1
fi

#ENDPOINT=http://193.107.127.194:6543/API/osug-doi/
ENDPOINT=https://emaa.osug.fr/API/osug-doi/

wget -r -np -nH ${ENDPOINT}

