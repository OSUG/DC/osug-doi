#!/bin/bash

# initialize environment:
source env.sh


if [ "$#" -eq  "0" ]
  then
    echo "missing project argument"
    exit 1
else
    PROJECT=$1
    DOI=$2
fi


# Publish DOIs (flag as public, generate landing pages then push to datacite):
$APP_CMD --action=publish --project=$PROJECT --doi=$DOI

if [ $? -ne 0 ]; then
  echo "java process failed"
  exit 1
fi

