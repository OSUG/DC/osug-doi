#!/bin/bash
# needs package: chardet 
DIR=../conf/

echo "checking encoding in folder $DIR"

for f in `find $DIR -name '*.csv'`; do
    filename=`basename $f`
    echo "checking encoding: $f"

    DET_OUT=$(chardet $f)
    ENC=$(echo $DET_OUT | sed "s|^.*: \(.*\) (confid.*$|\1|")

    if [ "$ENC" != "utf-8" ]; then
        echo "converting from $ENC to UTF-8"
        iconv -f "$ENC" -t utf8 "$f" -o "$f.txt"
        if [ $? -ne 0 ]; then
            echo "iconv failed"
            exit 1
	fi
	mv "$f.txt" "$f"
    fi
done

echo "done."

