<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd">

    <xsl:output method="xml" indent="no" omit-xml-declaration="no" media-type="text/html" encoding="utf-8"
                doctype-public="-//W3C//DTD XHTML 1.1//EN"
                doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

    <!-- global parameters -->
    <xsl:param name="www_root" select="''"/>
    <xsl:param name="date" select="''"/>
    <xsl:param name="parent" select="''"/>
    <xsl:param name="current" select="''"/>

    <xsl:include href="./bootstrap_style.xsl"/>




    <!-- base template -->
    <xsl:template match="/">
        <xsl:call-template name="custom-style">
            <xsl:with-param name="page_title">
                OSUG DOI -
                <xsl:if test="$parent != ''">
                    <xsl:value-of select="$parent"/> - </xsl:if>
                <xsl:value-of select="$current"/> - Report
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <!-- container template -->
    <!-- same for index / report -->
    <xsl:template name="custom-container">
        <ol class="breadcrumb">
            <li>
                <a href="{$www_root}index.html">OSUG DOI</a>
            </li>
            <xsl:if test="$parent">
                <li>
                    <a href="{$www_root}{$parent}/index.html">
                        <xsl:value-of select="$parent"/>
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="$current">
                <li class="active">
                    <xsl:value-of select="$current"/>
                </li>
            </xsl:if>
        </ol>

        <div class="doi-template">
            <xsl:apply-templates />

            <div class="text-right small">
                <xsl:value-of select="$date"/>
            </div>
        </div>
    </xsl:template>


    <!-- report -->
    <xsl:template match="report">
        <xsl:choose>
            <xsl:when test="count(./doi) != 0">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Identifier</th>
                                <th>Description</th>
                                <!-- <th>Status</th> -->
                                <th>Ext. URL</th>
                                <!-- <th>DOI URL</th> -->
                                <!-- <th>Created</th> -->
                                <th>Updated</th>
                                <th>Valid</th>
                                <th>Log</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="./doi"/>
                        </tbody>
                    </table>
                </div>
            </xsl:when>
            <xsl:otherwise>
                That's All Folks !
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="doi">
        <tr>
            <td>
                <a href="{./identifier/text()}.html">
                    <xsl:value-of select="./identifier/text()"/>
                </a>
            </td>
            <td>
                <xsl:value-of select="./description/text()"/>
            </td>
            <!--
                        <td>
                            <xsl:value-of select="./status/text()"/>
                        </td>
            -->
            <td>
                <xsl:if test="./landingExtUrl">
                    <a href="{./landingExtUrl/text()}" target="_blank">
                        <xsl:value-of select="./landingExtUrl/text()"/>
                    </a>
                </xsl:if>
            </td>
            <!--
                        <td>
                            <a href="{./landingLocUrl/text()}" target="_blank">
                                <xsl:value-of select="./landingLocUrl/text()"/>
                            </a>
                        </td>
            -->
            <!--
                        <td>
                            <xsl:value-of select="./createDate/text()"/>
                        </td>
            -->
            <td>
                <xsl:value-of select="./updateDate/text()"/>
            </td>
            <td class="vmid">
                <xsl:choose>
                    <xsl:when test="./valid/text() = 'true'">
                        <div class="label label-success">yes</div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="label label-danger">no</div>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
            <td class="vmid">
                <xsl:if test="./log/text() != ''">
                    <a role="button" class="btn btn-primary btn-xs" data-container="body"
                       tabindex="0" data-toggle="popover" data-trigger="focus"
                       data-placement="left" data-content="{./log/text()}">log</a>
                </xsl:if>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>
