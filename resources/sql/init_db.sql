-- defines OSUG-DOI group with users :

CREATE ROLE osug_doi_group WITH NOSUPERUSER NOCREATEDB NOCREATEROLE NOLOGIN;
ALTER ROLE osug_doi_group SET client_encoding = 'UTF8';

CREATE ROLE osug_doi_admin WITH LOGIN PASSWORD 'admin' INHERIT IN ROLE osug_doi_group;


-- creates OSUG-DOI database owned by osug_doi_super :

CREATE DATABASE osug_doi WITH OWNER = osug_doi_admin;

